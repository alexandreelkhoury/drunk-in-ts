import express, { Request, Response } from "express";
import { ObjectId } from "mongodb";
import { collections } from "../services/database.service";
import Bars from "../models/bars";

export const barsRouter = express.Router();

barsRouter.use(express.json());

barsRouter.get("/", async (_req: Request, res: Response) => {
    try {
        const bars = await collections.bars?.find({}).toArray();

        res.status(200).send(bars);
    } catch (error: unknown) {
        res.status(500).send((error as Error).message);
    }
});

barsRouter.get("/:id", async (req: Request, res: Response) => {
    const id = req?.params?.id;

    try {
        
        const query = { _id: new ObjectId(id) };
        const bars = await collections.bars?.findOne(query);

        if (bars) {
            res.status(200).send(bars);
        }
    } catch (error) {
        res.status(404).send(`Unable to find matching document with id: ${req.params.id}`);
    }
});

barsRouter.post("/", async (req: Request, res: Response) => {
    try {
        const newBar = req.body as Bars;
        const result = await collections.bars?.insertOne(newBar);

        result
            ? res.status(201).send(`Successfully created a new game with id ${result.insertedId}`)
            : res.status(500).send("Failed to create a new game.");
    } catch (error) {
        console.error(error);
        res.status(400).send((error as Error).message);
    }
});

barsRouter.put("/:id", async (req: Request, res: Response) => {
    const id = req?.params?.id;

    try {
        const updatedBar: Bars = req.body as Bars;
        const query = { _id: new ObjectId(id) };
      
        const result = await collections.bars?.updateOne(query, { $set: updatedBar });

        result
            ? res.status(200).send(`Successfully updated game with id ${id}`)
            : res.status(304).send(`Game with id: ${id} not updated`);
    } catch (error:unknown) {
        console.error((error as Error).message);
        res.status(400).send((error as Error).message);
    }
});

barsRouter.delete("/:id", async (req: Request, res: Response) => {
    const id = req?.params?.id;

    try {
        const query = { _id: new ObjectId(id) };
        const result = await collections.bars?.deleteOne(query);

        if (result && result.deletedCount) {
            res.status(202).send(`Successfully removed game with id ${id}`);
        } else if (!result) {
            res.status(400).send(`Failed to remove game with id ${id}`);
        } else if (!result.deletedCount) {
            res.status(404).send(`Game with id ${id} does not exist`);
        }
    } catch (error) {
        console.error((error as Error).message);
        res.status(400).send((error as Error).message);
    }
});