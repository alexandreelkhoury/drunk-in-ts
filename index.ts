// import express from "express";
// import { connectToDatabase } from "./services/database.service"
// import { barsRouter } from "./routes/bars.router";
// import { cocktailsRouter } from "./routes/cocktails.router";

// connectToDatabase()
//     .then(() => {
//         app.use("/cocktails", cocktailsRouter);

//         app.listen(port, () => {
//             console.log(`Server started at http://localhost:${port}`);
//         });
//     })
//     .catch((error: Error) => {
//         console.error("Database connection failed", error);
//         process.exit();
//     });

import express from "express";
import { connectToDatabase } from "./services/database.service";
import { barsRouter } from "./routes/bars.router";
import { cocktailsRouter } from "./routes/cocktails.router";

const app = express();
const port = 3000;

// Connect to the database and initialize the collections
connectToDatabase()
  .then(() => {
    // Use the routers with their respective endpoints
    app.use("/bars", barsRouter);
    app.use("/cocktails", cocktailsRouter);

    // Start the server
    app.listen(port, () => {
      console.log(`Server started at http://localhost:${port}`);
    });
  })
  .catch((error:any) => {
    console.error("Server initialization failed:", error);
    process.exit(1);
  });
