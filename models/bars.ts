import { ObjectId } from "mongodb";

export default class Bars {
    constructor(public location: { lat: number; lon: number }, public address: number, public cocktails: string[], public id?: ObjectId) {}
}
