import { ObjectId } from "mongodb";

export default class Cocktails {
    constructor(public name: string, public price: number, public alcool: boolean, public ingredients: string, public description: string, public id?: ObjectId) {}
}
