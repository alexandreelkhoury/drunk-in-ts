import * as mongoDB from "mongodb";
const { MongoClient, ServerApiVersion } = require('mongodb');
import * as dotenv from "dotenv";

export const collections: { bars?: mongoDB.Collection, cocktails?: mongoDB.Collection } = {}

dotenv.config();

export async function connectToDatabase () {

    const client = new MongoClient(process.env.URI, {
        serverApi: {
          version: ServerApiVersion.v1,
          strict: true,
          deprecationErrors: true,
        }
    });

    try {
        await client.connect();
        // Send a ping to confirm a successful connection
        await client.db("admin").command({ ping: 1 });
        console.log("Pinged your deployment. You successfully connected to MongoDB!");
      } finally {
        // Ensures that the client will close when you finish/error
        await client.close();
      }  
 }

 connectToDatabase().catch(console.dir);
